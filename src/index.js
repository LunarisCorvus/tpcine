import 'bootstrap'
import './assets/scss/style.scss'

autocomplete({
    input: document.getElementById("input"),
    minLength: 2,
    preventSubmit: true,
    className: "",
    fetch: (text, update) => {
        text = text.toLowerCase();
        fetch(`https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&language=fr-FR&query=${text}&page=1&include_adult=false`)
        .then ((film) => {
            if (film.ok) {
                return film.json();
            }
        })
        .then ((liste) => {
            liste.results
            console.log(liste.results)
            let table = [];
            for(let i=0 ; i < liste.results.length ; i++){
                table.push(liste.results[i].title);
            }
            update(table)
        })
        
    },
    render: (item) => {
        let div = document.createElement("div");
        div.textContent = item;
        return div;
    }
});